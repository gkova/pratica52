/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author gabriel
 * @param <T>
 */
// T é o parâmetro de tipo da classe
public class Equacao2Grau<T extends Number & Comparable<T>> { /// & Comparable<T>
    T info; // variável do tipo T
    private T a, b, c;
    private static final String MSG_Coeficiente_a_igual_0 = "Coeficiente a não pode ser zero";
    private static final String MSG_Delta_igual_0 = "Equação não tem solução real";

    // construtor padrão

    /**
     *
     * @param coefA
     * @param coefB
     * @param coefC
     */
    public Equacao2Grau(T coefA, T coefB, T coefC) {
        if (coefA == null || (coefA.intValue()) == 0)
            throw new RuntimeException(MSG_Coeficiente_a_igual_0);
        a = coefA;
        b = coefB;
        c = coefC;
    }
    
    // construtor recebe argumento do tipo T
    public Equacao2Grau(T info) {
        this.info = info;
    }

    public Equacao2Grau() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    // método retorna valor do tipo T
    public T getInfo() {
        return info;
    }

    // método recebe argumento do tipo T
    public void setInfo(T info) {
        this.info = info;
    }
    
    public void setA(T coefA){
        if (coefA == null || (coefA.intValue()) == 0)
            throw new RuntimeException(MSG_Coeficiente_a_igual_0);
        a = coefA;
    }
    
    public void setB(T coefB){
        b = coefB;
    }
    
    public void setC(T coefC){
        c = coefC;
    }
    
    public T getA(){
        return a;
    }
    
    public T getB(){
        return b;
    }
    
    public T getC(){
        return c;
    }
    
    public double getRaiz1()
    {
        double delta;
        delta = b.doubleValue()*b.doubleValue()-4*a.doubleValue()*c.doubleValue();
        
        if (delta < 0)
            throw new RuntimeException(MSG_Delta_igual_0);
        return ((-b.doubleValue()+Math.sqrt(delta))/(2*a.doubleValue()));
    }
    
    public double getRaiz2()
    {
        double delta;
        delta = b.doubleValue()*b.doubleValue()-4*a.doubleValue()*c.doubleValue();
        
        if (delta < 0)
            throw new RuntimeException(MSG_Delta_igual_0);
        return ((-b.doubleValue()-Math.sqrt(delta))/(2*a.doubleValue()));
    }
} 
