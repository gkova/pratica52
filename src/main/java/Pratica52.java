
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) {
        Equacao2Grau<Integer> e2g = new Equacao2Grau<Integer>(1, 3, 4);
        
        System.out.println(e2g.getA());
        System.out.println(e2g.getB());
        System.out.println(e2g.getC());
        System.out.println("Raiz 1:" + e2g.getRaiz1());
        System.out.println("Raiz 2:" + e2g.getRaiz2());
    }
}
